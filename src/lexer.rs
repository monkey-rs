use crate::Token;
use std::iter::Peekable;
use std::str::Chars;

pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>,
}

impl<'a> Lexer<'a> {
    pub fn new(input: &'a str) -> Lexer<'a> {
        Lexer {
            input: input.chars().peekable(),
        }
    }

    fn read_char(&mut self) -> Option<char> {
        self.input.next()
    }

    fn peek_char(&mut self) -> Option<&char> {
        self.input.peek()
    }

    fn eat_whitespace(&mut self) {
        while let Some(&c) = self.peek_char() {
            if c.is_whitespace() {
                let _ = self.read_char();
            } else {
                break;
            }
        }
    }

    fn read_identifier(&mut self, c: char) -> String {
        let mut ident = String::new();
        ident.push(c);
        while self.peek_char().filter(|c| c.is_alphanumeric()).is_some() {
            ident.push(self.read_char().unwrap());
        }
        ident
    }

    fn read_number(&mut self, c: char) -> i64 {
        let mut digits = String::new();
        digits.push(c);
        while self.peek_char().filter(|c| c.is_numeric()).is_some() {
            digits.push(self.read_char().unwrap());
        }
        digits.parse::<i64>().unwrap()
    }

    fn read_biop(&mut self, c: char) -> char {
        match c {
            '!' => {
                if self.peek_char().unwrap() == &'=' {
                    self.read_char();
                    '≠'
                } else {
                    '!'
                }
            }
            '=' => {
                if self.peek_char().unwrap() == &'=' {
                    self.read_char();
                    '⩵'
                } else {
                    '='
                }
            }
            _ => {
                panic!("n0pe")
            }
        }
    }
}

fn lookup_keyword(ident: String) -> Token {
    // lowercasing may be costly...
    match ident.to_lowercase().as_ref() {
        "fn" | "let" | "true" | "false" | "if" | "else" | "return" => {
            Token::Keyword(ident.to_uppercase())
        }
        _ => Token::Ident(ident),
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;
    fn next(&mut self) -> Option<Token> {
        self.eat_whitespace();
        if let Some(c) = self.read_char() {
            match c {
                '0'..='9' => Some(Token::Int(self.read_number(c))),
                '!' | '=' => Some(Token::Operator(self.read_biop(c))),
                '+' | '-' | '/' | '*' => Some(Token::Operator(c)),
                ';' | ',' | '(' | ')' | '{' | '}' | '<' | '>' => Some(Token::Punct(c)),
                '\0' => Some(Token::Eof),
                _ => {
                    if c.is_alphabetic() {
                        Some(lookup_keyword(self.read_identifier(c)))
                    } else {
                        Some(Token::Illegal(c))
                    }
                }
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn lex_ops_and_punct() {
        let input = "=+(){};,";
        let sut = Lexer::new(input);
        let expected = vec![
            Token::Operator('='),
            Token::Operator('+'),
            Token::Punct('('),
            Token::Punct(')'),
            Token::Punct('{'),
            Token::Punct('}'),
            Token::Punct(';'),
            Token::Punct(','),
        ];
        assert_eq!(sut.collect::<Vec<Token>>(), expected);
    }

    #[test]
    fn lex_biquals() {
        let input = "a == 3";
        let sut = Lexer::new(input);
        let expected = vec![
            Token::Ident(String::from("a")),
            Token::Operator('⩵'),
            Token::Int(3),
        ];
        assert_eq!(sut.collect::<Vec<Token>>(), expected);
    }

    #[test]
    fn lex_notquals() {
        let input = "a != 3";
        let sut = Lexer::new(input);
        let expected = vec![
            Token::Ident(String::from("a")),
            Token::Operator('≠'),
            Token::Int(3),
        ];
        assert_eq!(sut.collect::<Vec<Token>>(), expected);
    }

    #[test]
    fn lex_some_basic_src() {
        let input = "
let five = 5;
let ten = 10;
let add = fn(x, y) {
     x + y;
};
let result = add(five, ten);
";
        let expected = vec![
            Token::Keyword(String::from("LET")),
            Token::Ident(String::from("five")),
            Token::Operator('='),
            Token::Int(5),
            Token::Punct(';'),
            Token::Keyword(String::from("LET")),
            Token::Ident(String::from("ten")),
            Token::Operator('='),
            Token::Int(10),
            Token::Punct(';'),
            Token::Keyword(String::from("LET")),
            Token::Ident(String::from("add")),
            Token::Operator('='),
            Token::Keyword(String::from("FN")),
            Token::Punct('('),
            Token::Ident(String::from("x")),
            Token::Punct(','),
            Token::Ident(String::from("y")),
            Token::Punct(')'),
            Token::Punct('{'),
            Token::Ident(String::from("x")),
            Token::Operator('+'),
            Token::Ident(String::from("y")),
            Token::Punct(';'),
            Token::Punct('}'),
            Token::Punct(';'),
            Token::Keyword(String::from("LET")),
            Token::Ident(String::from("result")),
            Token::Operator('='),
            Token::Ident(String::from("add")),
            Token::Punct('('),
            Token::Ident(String::from("five")),
            Token::Punct(','),
            Token::Ident(String::from("ten")),
            Token::Punct(')'),
            Token::Punct(';'),
        ];
        let sut = Lexer::new(input);
        assert_eq!(sut.collect::<Vec<Token>>(), expected);
    }

    #[test]
    fn lex_some_more_complex_src() {
        let input = "let result = add(five, ten); !-/*5;
                    5 < 10 > 5;
                    if (5 < 10) {
                        return true;
                    } else {
                        return false;
                    }";
        let sut = Lexer::new(input);
        let result = sut.collect::<Vec<Token>>();

        assert!(
            !result.iter().any(|t| t.is_illegal()),
            "\nresult had illegal tokens \n{:?}",
            result
        )
    }
}
