extern crate clap;
use clap::{App, AppSettings, Arg, SubCommand};
mod repl;
use repl::repl;

fn main() {
    let app = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(Arg::with_name("INPUT").help("file on disk to process"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(SubCommand::with_name("repl").about("a read-eval-print-loop"));
    let matches = app.get_matches();
    match matches.subcommand() {
        ("repl", Some(_sub_matches)) => repl(),
        _ => {}
    }
}
