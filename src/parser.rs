use std::error::Error;

use crate::lexer;
use crate::Expression;
use crate::Program;
use crate::Statement;
use crate::Token;
use std::fmt;

#[derive(Debug, Clone)]
pub struct ParseError {
    err: String,
}

impl Error for ParseError {}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "parse error: {:?}", self.err)
    }
}

#[derive(Debug)]
struct ParseErrorCollection(Vec<ParseError>);

impl fmt::Display for ParseErrorCollection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for e in &self.0 {
            writeln!(f, "{}", e)?;
        }
        Ok(())
    }
}

impl Error for ParseErrorCollection {
    fn description(&self) -> &str {
        "error in one of the addends"
    }
}

// since we don't know where we're going with this yet, to begin, the
// parser will match the go impl.
// jk right out the gate we're skipping the lexer cause ownership of it is weird
// instead we're just take a vector of tokens...
pub struct Parser {
    tokens: Vec<Token>,
    cur: usize,
}

impl Parser {
    fn new(lexer: &mut lexer::Lexer) -> Self {
        Parser {
            tokens: lexer.collect(),
            cur: 0,
        }
    }

    // parser is stateful
    fn next(&mut self) -> &Token {
        self.cur += 1;
        &self.tokens[self.cur]
    }

    fn current(&self) -> Token {
        self.tokens[self.cur].clone()
    }

    fn peek(&self) -> Token {
        self.tokens[self.cur + 1].clone()
    }

    fn expect_peek(&mut self, example: Token, discrim: bool) -> Result<Token, ParseError> {
        let peek = self.peek();
        let we_match: bool = if discrim {
            std::mem::discriminant(&example) == std::mem::discriminant(&peek)
        } else {
            example == peek
        };
        if we_match {
            self.next();
            Ok(peek)
        } else {
            Err(ParseError {
                err: String::from(format!("expected a {:?} but got a {:?}", example, peek)),
            })
        }
    }

    fn parse_statement(&mut self, token: &str) -> Result<Statement, ParseError> {
        match token {
            "FN" => panic!("kw fn"),
            "LET" => {
                self.expect_peek(Token::Ident("_".to_string()), true)?;
                if let Token::Ident(ident_str) = self.current() {
                    self.expect_peek(Token::Operator('='), false)?;
                    loop {
                        let z = self.next();
                        if let Token::Punct(';') = z {
                            break;
                        }
                    }
                    Ok(Statement::Let(
                        ident_str,
                        Expression::Identifier(String::from("mmm")),
                    ))
                } else {
                    unreachable!()
                }
            }
            _ => Err(ParseError {
                err: format!("no really, unknown keyword? get it together bro {}", token),
            }),
        }
    }

    fn parse(&mut self) -> Result<Program, ParseErrorCollection> {
        // this loop is obvious garbage but i can't figure out
        // how to make the ownership work another way yet, so
        // we're moving on!
        // specifically we can't call next() until we try and parse token0
        let mut stmts: Vec<Statement> = Vec::new();
        let mut errs: Vec<ParseError> = Vec::new();
        loop {
            let t = self.current();
            if let Token::Eof = t {
                break;
            }
            if let Token::Keyword(kw) = t {
                match self.parse_statement(&kw) {
                    Ok(stmt) => stmts.push(stmt),
                    Err(e) => errs.push(e),
                }
            }
            self.next();
        }
        if !errs.is_empty() {
            return Err(ParseErrorCollection(errs));
        }
        Ok(Program { statements: stmts })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_parse() {
        let input = "let avar = 10;\0";
        let stmts = vec![Statement::Let(
            String::from("avar"),
            Expression::IntegerLiteral(10),
        )];
        let mut p = Parser::new(&mut lexer::Lexer::new(input));
        match p.parse() {
            Ok(Program { statements: kn }) => assert_eq!(kn, stmts),
            Err(_) => panic!("shouldn't have errored while parsing"),
        }
    }

    #[test]
    fn err_parse() {
        let input = "let avar && 10;\0";
        let mut p = Parser::new(&mut lexer::Lexer::new(input));
        p.parse().expect_err("expected parsing to fail");
    }
}
