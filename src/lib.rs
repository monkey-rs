#![allow(dead_code)]

#[derive(Clone, Debug, PartialEq)]
pub enum Token {
    Illegal(char),
    Eof,
    Ident(String),
    Int(i64),
    Float(String),
    String(String),
    Operator(char),
    Keyword(String),
    Punct(char),
}

impl Token {
    #[cfg(test)]
    fn is_illegal(&self) -> bool {
        match *self {
            Token::Illegal(..) => true,
            _ => false,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Program {
    statements: Vec<Statement>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Statement {
    Let(String, Expression),
    Expression(Expression),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Expression {
    Identifier(String),
    IntegerLiteral(i64),
}

pub mod lexer;
pub mod parser;
