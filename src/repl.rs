use monkey::lexer::Lexer;
use std::io;
use std::io::Write;

//unwraps should be ?s use try/result
pub fn repl() {
    let stdin = io::stdin();
    loop {
        print!(">>> ");
        io::stdout().flush().unwrap();

        let mut line = String::new();
        stdin.read_line(&mut line).unwrap();
        line = line.trim_end().to_string();
        if line == "EXIT" {
            break;
        } else {
            let sut = Lexer::new(&line);
            for token in sut {
                print!("{:?} ", token);
            }
            println!("");
        }
    }
}
